import React, { Component } from 'react'
import withRedux from "next-redux-wrapper"
import makeStore  from '../server/core/store/store'
import WizardLayout from '../server/core/install/layout/WizardLayout'
import WizardContainer from '../server/core/install/container/WizardContainer';
import "../server/core/styling/styles.scss"

class Install extends Component {


  constructor(props){
    super(props)
    this.state={}
  }

  render() {
    return(
      <WizardLayout>
        <WizardContainer />
      </WizardLayout>
    )
  }
}


export default withRedux(makeStore, null)(Install)