import React, { Component } from 'react'
import fetch from 'isomorphic-unfetch'
import withRedux from "next-redux-wrapper"
import { Link } from '../server/core/routes/routes'
import makeStore  from '../server/core/store/store'
import AdminLayout from '../server/core/layouts/AdminLayout'
import PagesContainer from '../server/core/containers/PagesContainer'

class Pages extends Component {

  static async getInitialProps({store}) {
    const res = await fetch('http://localhost:3000/page')
    const pages = await res.json()
    store.dispatch({type: 'GET_PAGES_SUCCESS', pages: pages});
    return { pages }
  }

  render(){
    return(
      <AdminLayout>
        <h2>Content Pages</h2>
        <Link route="/admin/pages/create">
          <a className="btn btn-scp-blue">Add new Page</a>
        </Link>
        <PagesContainer pages={this.props.pages} />
      </AdminLayout>
    )
  }
}

export default withRedux(makeStore,null)(Pages)