import React, { Component } from 'react'
import withRedux from 'next-redux-wrapper'
import makeStore from '../server/core/store/store'
import SignInContainer from '../server/core/containers/auth/SignInContainer'
import '../server/core/styling/styles.scss'

class SingIn extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  render() {
    return <SignInContainer />
  }
}

export default withRedux(makeStore, null)(SingIn)
