import React, { Component } from 'react'
import withRedux from 'next-redux-wrapper'
import makeStore from '../server/core/store/store'
import { Link } from '../server/core/routes/routes'
import '../server/core/styling/styles.scss'

class Index extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <div className="scp-page index">
        <div>
          <div className="scp-logo-index-container">
            <img src="./static/scp-icon-b.png" alt="homepage" />
          </div>
          <h1>Welcome to Scorpion CMS</h1>
          <p>We are are happy to have you here.</p>
          <Link route="/install">
            <a className="btn btn-scp-blue">Install</a>
          </Link>
          <Link route="/login">
            <a className="btn btn-border-only-info">Login</a>
          </Link>
        </div>
      </div>
    )
  }
}

export default withRedux(makeStore, null)(Index)
