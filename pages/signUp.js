import React, { Component } from 'react'
import withRedux from 'next-redux-wrapper'
import makeStore from '../server/core/store/store'
import SignUpContainer from '../server/core/containers/auth/SignUpContainer'
import '../server/core/styling/styles.scss'

class SingUp extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <div>
        <SignUpContainer />
      </div>
    )
  }
}

export default withRedux(makeStore, null)(SingUp)
