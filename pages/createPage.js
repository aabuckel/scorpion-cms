import React, { Component } from 'react'
import withRedux from 'next-redux-wrapper'
import makeStore from '../server/core/store/store'
import AdminLayout from '../server/core/layouts/AdminLayout'
import CreatePageContainer from '../server/core/containers/createPageContainer';
import "../server/core/styling/styles.scss"

class createPage extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  render() {
    return (
      <AdminLayout>
        <h2>Create a Page Type</h2>
        <CreatePageContainer />
      </AdminLayout>
    )
  }
}

export default withRedux(makeStore, null)(createPage)
