import React, { Component } from 'react'
import withRedux from 'next-redux-wrapper'
import makeStore from '../server/core/store/store'
import AdminLayout from '../server/core/layouts/AdminLayout'
import "../server/core/styling/styles.scss"

class Admin extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    return (
      <div>
        <AdminLayout>

        </AdminLayout>
      </div>
    )
  }
}

export default withRedux(makeStore, null)(Admin)
