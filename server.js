const express = require('express')
require('dotenv').config()
const compression = require('compression')
const next = require('next')
var bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
var expressValidator = require('express-validator')

// API ROUTES IMPORTS
const SettingsRoutes = require('./server/core/routes/settings/settings.routes')
const UserRoutes = require('./server/core/routes/user/user.routes')
const BlockRoutes = require('./server/core/routes/block/block.routes')
const TaxonomyRoutes = require('./server/core/routes/taxonomy/taxonomy.routes')
const PageRoutes = require('./server/core/routes/page/page.routes')
const MenuRoutes = require('./server/core/routes/menu/menu.routes')

// Middlewares
const authorized = require('./server/core/middlewares/authorized.middleware')
const unauthorized = require('./server/core/middlewares/unauthorized.middleware')

const port = process.env.PORT || 3000
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })

const handler = app.getRequestHandler()

app.prepare().then(() => {
  const server = express()
  server.disable('x-powered-by')
  server.use(cookieParser())
  server.use(bodyParser.json())
  server.use(expressValidator())
  server.use(compression())

  // GUARD CHECKERS
  server.get('/', authorized, (req, res) => {
    const actualPage = '/'
    return app.render(req, res, actualPage)
  })

  server.get('/install', authorized, (req, res) => {
    const actualPage = '/install'
    return app.render(req, res, actualPage)
  })

  server.get('/signUp', authorized, (req, res) => {
    const actualPage = '/signUp'
    return app.render(req, res, actualPage)
  })

  server.get('/admin', unauthorized, (req, res) => {
    const actualPage = '/admin'
    return app.render(req, res, actualPage)
  })

  server.get('/admin/blocks', unauthorized, (req, res) => {
    const actualPage = '/admin/blocks'
    return app.render(req, res, actualPage)
  })

  server.get('/admin/pages', unauthorized, (req, res) => {
    const actualPage = '/admin/pages'
    return app.render(req, res, actualPage)
  })

  server.get('/admin/pages/create', unauthorized, (req, res) => {
    const actualPage = '/admin/pages/create'
    return app.render(req, res, actualPage)
  })

  // API ROUTES
  server.use('/settings', SettingsRoutes)
  server.use('/auth', UserRoutes)
  server.use('/block', BlockRoutes)
  server.use('/taxomony', TaxonomyRoutes)
  server.use('/page', PageRoutes)
  server.use('/menu', MenuRoutes)

  // NEXT ROUTER HANDLER
  server.get('*', (req, res) => {
    return handler(req, res)
  })

  startServer()

  function startServer() {
    server.listen(port, () => {
      console.log(`> Express server running on http://localhost:${port}`)
    })
  }
})
