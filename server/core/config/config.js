module.exports = {
  env: process.env.NODE_ENV || 'dev',
  db_string: process.env.DB_URL || 'mongodb://localhost:27017/scorpion',
  key: process.env.API_KEY || 'scp',
  endpoint: process.env.API_ENDPOINT || 'http://localhost:3000/'
}
