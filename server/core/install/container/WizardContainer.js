import React, { Component } from 'react'
import { connect } from 'react-redux'
import WizardStatus from '../components/WizardStatus'
// import StepWizardHandler from '../components/StepWizardHandler'
import StepWelcome from '../components/StepWelcome'
import StepSiteConfig from '../components/StepSiteConfig'
import StepComplete from '../components/StepComplete'
import { nextStep, previousStep, setSettings } from '../../store/actions/wizard'

class WizardContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      dbName: '',
      siteName: '',
      name: '',
      password: '',
      email: ''
    }
  }

  incrementCounter = () => {
    this.props.nextStep(this.props.counter)
  }

  decrementCounter = () => {
    this.props.previousStep(this.props.counter)
  }

  render() {
    let step = null
    if (this.props.counter === 0) {
      step = <StepWelcome incrememtCounter={this.incrementCounter} />
    }
    if (this.props.counter === 1) {
      step = (
        <StepSiteConfig
          errors={this.props.formErrors}
          onSuccess={this.props.setSettings}
          decrementCounter={this.decrementCounter}
          incrementCounter={this.incrementCounter}
          settings={this.state}
        />
      )
    }

    if (this.props.counter === 2) {
      step = <StepComplete config={this.props.config} />
    }

    return (
      <section className="scp-wizard box rounded">
        <aside className="scp-install-status">
          <WizardStatus counter={this.props.counter} />
        </aside>
        <div className="scp-install-step">{step}</div>
      </section>
    )
  }
}

function mapStateToProps(state) {
  return {
    counter: state.wizardReducer.counter,
    config: state.wizardReducer.config,
    formErrors: state.wizardReducer.formErrors
  }
}

function mapDispatchToProps(dispatch) {
  return {
    nextStep: counter => dispatch(nextStep(counter)),
    previousStep: counter => dispatch(previousStep(counter)),
    setSettings: config => dispatch(setSettings(config))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(WizardContainer)
