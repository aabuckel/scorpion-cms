import React from "react";

const WizardLayout = props => (
  <div>
    {props.children}
  </div>
);

export default WizardLayout;
