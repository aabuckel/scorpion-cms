import React from 'react'
import PropTypes from 'prop-types'
import { Link } from '../../routes/routes'

const StepWizardHandler = props => {
  const {
    counter,
    decrementCounter,
    incrememtCounter,
    onPreSuccess,
    onSuccess
  } = props
  return (
    <div className="scp-wizard-handler">
      <button className="btn btn-danger" onClick={decrementCounter}>
        Previous
      </button>
      {counter === 1 ? (
        <a className="btn btn-success" onClick={onSuccess}>
          Install
        </a>
      ) : (
        <button className="btn btn-scp-blue" onClick={incrememtCounter}>
          Next
        </button>
      )}
    </div>
  )
}

StepWizardHandler.propTypes = {
  counter: PropTypes.number.isRequired,
  decrementCounter: PropTypes.func.isRequired,
  incrememtCounter: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired
}

export default StepWizardHandler
