import React from 'react'
import { Router } from '../../routes/routes'

const StepComplete = props => {
  Router.pushRoute('/admin')
  return (
    <div>
      <h2>Installation Completed</h2>
      <p>You have succesfuly completed the installation.</p>
    </div>
  )
}

export default StepComplete
