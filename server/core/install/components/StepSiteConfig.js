import React from 'react'
import { withFormik } from 'formik'

const StepSiteConfig = ({
  values,
  errors,
  touched,
  handleChange,
  handleBlur,
  handleSubmit,
  isSubmitting,
  onSuccess,
  decrementCounter,
  incrementCounter
}) => {
  return (
    <div>
      <h2>Website Settings</h2>
      <p className="alert alert-info">
        Almost done, we need to set up your website first.
      </p>
      <form onSubmit={handleSubmit}>
        <div className="input-group">
          <label>Database Name</label>
          <input
            type="text"
            placeholder="Database name"
            name="dbName"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.dbName}
          />
          {touched.dbName &&
            errors.dbName && (
              <div className="status danger">{errors.dbName}</div>
            )}
        </div>

        <div className="input-group">
          <label>Site Name</label>
          <input
            type="text"
            placeholder="Site name"
            name="siteName"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.siteName}
          />
          {touched.siteName &&
            errors.siteName && (
              <div className="status danger">{errors.siteName}</div>
            )}
        </div>

        <div className="input-group">
          <label>Name</label>
          <input
            type="text"
            placeholder="Name"
            name="name"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.name}
          />
          {touched.name &&
            errors.name && <div className="status danger">{errors.name}</div>}
        </div>

        <div className="input-group">
          <label>Password</label>
          <input
            type="password"
            placeholder="Password"
            name="password"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.password}
          />
          {touched.password &&
            errors.password && (
              <div className="status danger">{errors.password}</div>
            )}
        </div>

        <div className="input-group">
          <label>Email</label>
          <input
            type="email"
            placeholder="Email"
            name="email"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.email}
          />
          {touched.email &&
            errors.email && <div className="status danger">{errors.email}</div>}
        </div>

        <div className="input-group">
          <button className="btn btn-warning" onClick={decrementCounter}>
            Back
          </button>
          <input
            className="btn btn-info"
            type="submit"
            value="Install"
            disabled={isSubmitting}
          />
        </div>
      </form>
    </div>
  )
}

const formValidations = withFormik({
  mapPropsToValues: props => ({
    dbName: props.settings.dbName,
    siteName: props.settings.siteName,
    name: props.settings.name,
    password: props.settings.password,
    email: props.settings.email
  }),

  validate: (values, props) => {
    const errors = {}
    if (!values.dbName) {
      errors.dbName = 'Required'
    }
    if (!values.siteName) {
      errors.siteName = 'Required'
    }
    if (!values.name) {
      errors.name = 'Required'
    }
    if (!values.password) {
      errors.password = 'Required'
    }
    if (
      !/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/g.test(
        values.password
      )
    ) {
      errors.password = 'Invalid password format'
    }
    if (!values.email) {
      errors.email = 'Required'
    }
    if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
      errors.email = 'Invalid email address'
    }
    return errors
  },

  handleSubmit: (values, { props, setSubmitting, setErrors }) => {
    props.onSuccess(values)
    props.incrementCounter()
  }
})(StepSiteConfig)

export default formValidations
