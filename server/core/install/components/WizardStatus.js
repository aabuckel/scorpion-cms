import React from 'react'

const WizardStatus = props => {
  const { counter } = props
  return (
    <div>
      <div className="scp-logo-status">
        <img src="/static/scp-icon-w.png" alt="scp logo" />
      </div>
      <h2>Scorpion CMS</h2>
      <ul>
        <li className={counter >= 0 ? 'status info' : ''}>Welcome</li>
        <li className={counter > 0 ? 'status info' : ''}>Site Configuration</li>
        <li className={counter > 1 ? 'status info' : ''}>
          Configuration Completed
        </li>
      </ul>
    </div>
  )
}

export default WizardStatus
