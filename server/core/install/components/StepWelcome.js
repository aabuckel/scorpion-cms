import React from 'react'
import PropTypes from 'prop-types'

const StepWelcome = props => {
  const { incrememtCounter } = props
  return (
    <div>
      <h2 className="example">Welcome to Scorpion CMS Installer</h2>
      <p>An Awesome CMS build websites faster than usual.</p>
      <button className="btn btn-scp-blue" onClick={incrememtCounter}>
        Start
      </button>
    </div>
  )
}

StepWelcome.propTypes = {
  incrememtCounter: PropTypes.func.isRequired
}

export default StepWelcome
