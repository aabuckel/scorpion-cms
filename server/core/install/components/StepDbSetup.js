import React from 'react'
import { Form, Input } from 'semantic-ui-react'

const StepDbSetup = props => {
  const { onChange } = props
  return (
    <Form>
      <h2>Database Setup</h2>
      <Form.Group>
        <Form.Field>
          <label>Name</label>
          <input
            type="text"
            placeholder="name"
            max={10}
            onChange={onChange}
            name="databaseName"
          />
        </Form.Field>
      </Form.Group>

      <Form.Group>
        <Form.Field>
          <label>User</label>
          <input
            type="text"
            placeholder="user"
            max={8}
            onChange={onChange}
            name="databaseUserName"
          />
        </Form.Field>
      </Form.Group>

      <Form.Group>
        <Form.Field>
          <label>Password</label>
          <input
            type="text"
            placeholder="password"
            max={12}
            onChange={onChange}
            name="databaseUserPassword"
          />
        </Form.Field>
      </Form.Group>
    </Form>
  )
}

export default StepDbSetup
