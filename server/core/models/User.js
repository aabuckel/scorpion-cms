var mongoose = require('mongoose')
const argon2 = require('argon2')

var { Schema } = mongoose

var UserSchema = new Schema({
  name: {
    type: String
  },
  email: {
    type: String
  },
  password: {
    type: String
  },
  status: {
    type: String
  },
  role: {
    name: { type: String }
  },
  isAdmin: {
    type: String
  },
  dateCreated: { type: Date, default: Date.now() },
  updatedAt: {
    type: Date
  }
})

UserSchema.pre('save', function(next) {
  let user = this

  argon2
    .hash(user.password)
    .then(hash => {
      user.password = hash
      next()
    })
    .catch(err => {
      return err
    })
})

var User = mongoose.model('User', UserSchema)
module.exports = User
