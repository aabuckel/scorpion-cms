var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var BlockSchema = new Schema({
  name: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  description: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  body: {
    type: String
  },
  author: {
    type: String
  },
  section: {
    type: String
  },
  dateCreated: { type: Date, default: Date.now }, 
});

var Block = mongoose.model('Block', BlockSchema);
module.exports = Block;