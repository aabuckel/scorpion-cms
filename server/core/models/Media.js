var mongoose = require('mongoose');

const uuid = require('uuid/v1');

var Schema = mongoose.Schema;

var MediaSchema = new Schema({
  name: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  description: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  item: {
    id: {
      type: Number,
      default: uuid()
    },
    mediaType: {
      type: String
    },
    name: {
      type: String,
      unique: true,
      required: true,
      trim: true
    },
    description: {
      type: String,
      unique: true,
      required: true,
      trim: true
    },
    ext: {
      type: String,
    },
    url: {
      type: String,
      require: true,
    }
  },
  dateCreated: { type: Date, default: Date.now },
});

var Media =  mongoose.model('Media', MediaSchema);
module.exports = Media;