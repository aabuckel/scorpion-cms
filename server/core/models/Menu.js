const mongoose = require('mongoose')

const { Schema } = mongoose

const MenuSchema = new Schema({
  name: {
    type: String,
    unique: true,
    required: true
  },
  section: {
    type: String,
    unique: true,
    required: true
  },
  theme: {
    type: String,
    unique: true,
  },
  items:[{
    _id: Schema.Types.ObjectId,
    name: {
      type: String,
      unique: true,
      required: true
    },
    link: {
      type: String,
      unique: true,
      required: true
    },
    dateCreated: {
      type: Date,
      default: Date.now() 
    }
  }],

  dateCreated: { type: Date, default: Date.now() }
});

var Menu = mongoose.model('Menu', MenuSchema);
module.exports = Menu;