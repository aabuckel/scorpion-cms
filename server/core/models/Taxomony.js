var mongoose = require("mongoose");

var { Schema } = mongoose;

var TaxomonySchema = new Schema({
  name: {
    type: String,
    unique: true
  },
  terms: [{
    _id: Schema.Types.ObjectId,
    name: {
      type: String
    },
    dateCreated: {
      type: Date,
      default: Date.now()
      }
  }],
  dateCreated: {
    type: Date,
    default: Date.now()
  }
});

var Taxonomy = mongoose.model("Taxonomy", TaxomonySchema);
module.exports = Taxonomy;
