var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var EntitySchema = new Schema({
  name: {
    type: String,
    unique: true,
    required: true
  },
  entityType: {
    type: String
  },
  fields: {
    type: []
  },
  dateCreated: { type: Date, default: Date.now }
});

var Entity = mongoose.model('Entity',EntitySchema);
module.exports = Entity;