const mongoose = require('mongoose');


const { Schema } = mongoose

const PageSchema = new Schema ({
  name: {
    type: String
  },
  description: {
    type: String
  },
  url:{
    type: String
  },
  content: {
    type: String
  },
  author: {
    type: String
  },
  theme: {
    type: String
  },
  dateCreated: { type: Date, default: Date.now() },

})

const Page = mongoose.model('Page', PageSchema)

module.exports = Page