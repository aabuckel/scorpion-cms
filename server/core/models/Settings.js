const mongoose = require('mongoose')

const { Schema } = mongoose

var SettingsSchema = new Schema({
  dbName: {
    type: String
  },
  siteName: {
    type: String
  },
  createdAt: {
    type: Date,
    default: Date.now()
  },
  updatedAt: {
    type: Date
  }
})

const Settings = mongoose.model('Settings', SettingsSchema)
module.exports = Settings
