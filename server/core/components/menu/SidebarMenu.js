import React, { Component } from 'react'
import { Link } from '../../routes/routes'

class SidebarMenu extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    const { isOpen } = this.props
    return (
      <div className={`sidebar-menu ${isOpen ? 'open' : 'closed'}`}>
        <nav>
          <div className="scp-logo-bg">
            <div className="scp-logo-container">
              <img src="/static/scp-icon-b.png" />
            </div>
          </div>
          <ul>
            <li>
              <Link route="/">
                <a className="scp-branding">SCORPION</a>
              </Link>
            </li>
            <li>
              <Link route="/admin">
                <a>Dasboard</a>
              </Link>
            </li>
            <li>
              <Link route="/admin/pages">
                <a>Pages</a>
              </Link>
            </li>
            <li>
              <Link route="/admin/blocks">
                <a>Blocks</a>
              </Link>
            </li>
            <li>
              <Link route="/menus">
                <a>Menus</a>
              </Link>
            </li>
            <li>
              <Link route="/taxonomies">
                <a>Taxonomies</a>
              </Link>
            </li>
            <li>
              <Link route="/users">
                <a>Users</a>
              </Link>
            </li>
            <li>
              <Link route="/settings">
                <a>Settings</a>
              </Link>
            </li>
          </ul>
        </nav>
      </div>
    )
  }
}

export default SidebarMenu
