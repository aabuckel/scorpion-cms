import React from 'react'
import { Link } from '../../routes/routes' 
import LogoutContainer from '../../containers/auth/LogoutContainer';

const NavigationMenu = () => {
  return (
    <nav className="header-menu">
      <ul>
        <Link route="/">
          <li>
            <a>Current User</a>
          </li>
        </Link>
        <li>
          <LogoutContainer />
        </li>
      </ul>
    </nav>
  )
}
export default NavigationMenu
