import React from 'react'
import { withFormik } from 'formik'
import { Link } from '../../routes/routes'

const SingUpForm = ({
  values,
  errors,
  touched,
  handleChange,
  handleBlur,
  handleSubmit,
  isSubmitting,
  onSubmit
}) => {
  return (
    <form className="form" onSubmit={handleSubmit}>
      <h2>Create an Account</h2>
      <div className="input-group">
        <label>Name</label>
        <input
          type="text"
          placeholder="name"
          id="name"
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.name}
        />
        {touched.name &&
          errors.name && <div className="status danger">{errors.name}</div>}
      </div>

      <div className="input-group">
        <label>Email</label>
        <input
          type="email"
          placeholder="email"
          name="email"
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.email}
        />
        {touched.email &&
          errors.email && <div className="status danger">{errors.email}</div>}
      </div>

      <div className="input-group">
        <label>Password</label>
        <input
          type="password"
          placeholder="password"
          name="password"
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.password}
        />
        {touched.password &&
          errors.password && (
            <div className="status danger">{errors.password}</div>
          )}
      </div>

      <div className="input-group">
        <label>Confirm password</label>
        <input
          type="password"
          placeholder="confirm password"
          name="confirm"
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.confirm}
        />
        {touched.confirm &&
          errors.confirm && (
            <div className="status danger">{errors.confirm}</div>
          )}
      </div>

      <div className="input-group">
        <input
          className="btn btn-info btn-wider"
          type="submit"
          value="Sign Up"
        />
      </div>
      <Link route="/login">
        <a>Already a member?</a>
      </Link>
    </form>
  )
}

const SingUpFormValidations = withFormik({
  mapPropsToValues: props => ({
    name: '',
    email: '',
    password: '',
    confirm: ''
  }),
  validate: (values, props) => {
    const errors = {}
    if (!values.name) {
      errors.name = 'A name is required'
    }
    if (!values.email) {
      errors.email = 'A email is required'
    }
    if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
      errors.email = 'Invalid email address'
    }
    if (!values.password) {
      errors.password = 'Required'
    }
    if (
      !/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/g.test(
        values.password
      )
    ) {
      errors.password = 'Invalid password format'
    }
    if (!values.confirm) {
      errors.confirm = 'A password confirmation is required'
    }
    if (values.password !== values.confirm) {
      errors.confirm = 'Passwords doesn"t match'
    }
    return errors
  },

  handleSubmit: (values, { props, setSubmitting, setErrors }) => {
    props.onSubmit(values)
  }
})(SingUpForm)

export default SingUpFormValidations
