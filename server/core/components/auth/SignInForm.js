import React from 'react'
import { withFormik } from 'formik'

const SingInForm = ({
  values,
  errors,
  touched,
  handleChange,
  handleBlur,
  handleSubmit,
  isSubmitting,
  onSubmit
}) => {
  return (
    <div>
      <h2>Sign In</h2>
      <form onSubmit={handleSubmit}>
        <div className="input-group">
          <label>Email</label>
          <input
            type="email"
            placeholder="email"
            name="email"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.email}
          />
          {touched.email &&
            errors.email && <div className="status danger">{errors.email}</div>}
        </div>

        <div className="input-group">
          <label>Password</label>
          <input
            type="password"
            placeholder="password"
            name="password"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.password}
          />
          {touched.password &&
            errors.password && (
              <div className="status danger">{errors.password}</div>
            )}
        </div>

        <input
          className="btn btn-info"
          type="submit"
          value="Sing In"
          disabled={isSubmitting}
        />
      </form>
    </div>
  )
}

const SingInFormValidations = withFormik({
  mapPropsToValues: props => ({
    password: '',
    email: ''
  }),

  validate: (values, props) => {
    const errors = {}
    if (!values.password) {
      errors.password = 'Required'
    }
    if (
      !/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/g.test(
        values.password
      )
    ) {
      errors.password = 'Invalid password format'
    }
    if (!values.email) {
      errors.email = 'Required'
    }
    if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
      errors.email = 'Invalid email address'
    }
    return errors
  },

  handleSubmit: (values, { props, setSubmitting, setErrors }) => {
    props.onSubmit(values)
  }
})(SingInForm)

export default SingInFormValidations
