const sanitizeHtml = require('sanitize-html')
require('dotenv').config()
const key = require('../../config/config')
const Settings = require('../../models/Settings')
const User = require('../../models/User')
var jwt = require('jsonwebtoken')
const mongoose = require('mongoose')
const fs = require('fs')
const { validationResult } = require('express-validator/check')

function getSettings(req, res, next) {
  Settings.find({}, function(err, settings) {
    if (err) {
      res.status(404).send('Settings not found')
      res.status(500).send('There was a problem getting settings')
    }
    res
      .status(200)
      .send(settings)
      .end()
  })
}

function createSettings(req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.mapped() })
  }

  const newSettings = {
    dbName: sanitizeHtml(req.body.dbName),
    siteName: sanitizeHtml(req.body.siteName)
  }

  const newUser = {
    name: sanitizeHtml(req.body.name),
    password: sanitizeHtml(req.body.password),
    email: sanitizeHtml(req.body.email),
    isAdmin: true
  }

  var settings = `
      NODE_ENV=dev
      PORT=3000
      DB_URL=mongodb://localhost:27017/${newSettings.dbName}
      API_ENDPOINT=http://localhost:3000/
      API_KEY='SCMS'
      `

  fs.writeFile('.env', settings, err => {
    if (err) throw err
  })

  mongoose.connect(`mongodb://localhost:27017/${newSettings.dbName}`)

  Settings.create(newSettings, function(err, settings) {
    if (err) res.status(500).send('There was a problem setting the website')

    User.create(newUser, function(err, user) {
      if (err) res.status(500).send('There was a problem setting the website')

      var token = jwt.sign({ id: user._id }, key.key, {
        expiresIn: '1hr'
      })
      res.status(200).send({ auth: true, token: token })
    })
  })
}

function updateSettings(req, res, next) {
  Settings.findByIdAndUpdate(req.params.id, req.body, { new: true }, function(
    err,
    settings
  ) {
    if (err)
      res
        .status(500)
        .send('There was a problem updating settings in the website')
    res.status(200).send(settings)
  })
}

module.exports = {
  getSettings,
  createSettings,
  updateSettings
}
