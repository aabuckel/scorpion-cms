const sanitizeHtml = require('sanitize-html')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')

const Page = require('../../models/Page')

function getPages(req, res, next){
  Page.find({}, function(err, pages){
    if(err) res.status(500).send('there was a problem retrieving all pages')
    res.status(200).send(pages)
  })
}

function getPage(req, res, next){
  Page.findById(req.params.pageId, function(err, pages){
    if(err) res.status(500).send('there was a problem retrieving the page')
    res.status(200).send(pages)
  })
}

function createPage(req, res, next){
    if(
    req.body.name &&
    req.body.description &&
    req.body.url &&
    req.body.content &&
    req.body.author &&
    req.body.theme
  ){

    const newPage = {
      name: sanitizeHtml(req.body.name),
      description: sanitizeHtml(req.body.description),
      url: sanitizeHtml(req.body.url),
      content: sanitizeHtml(req.body.content),
      author: sanitizeHtml(req.body.author),
      theme: sanitizeHtml(req.body.theme)
    }

    Page.create(newPage, function(err, page) {
      if(err) res.status(500).send('there was a problem creating the page')
      res.status(200).send(page)
    })
  }
}
function deletePage(req, res, next){
  Page.findByIdAndRemove(req.params.pageId, function(err, page){
    if (err) res.status(500).send('there was a problem deleting the page')
    res.status(200).send('Page deleted')
  })
}

function updatePage(req, res, next){
  Page.findByIdAndUpdate(req.params.pageId, req.body, {new: true}, function(err, page){
    if(err) res.status(500).send('there was a problem updating the page')
    res.status(200).send(page)
  })
}

module.exports = {
  getPages,
  getPage,
  createPage,
  deletePage,
  updatePage
}
