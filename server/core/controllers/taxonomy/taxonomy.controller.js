const sanitizeHtml = require('sanitize-html')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')

var Taxonomy = require('../../models/Taxomony')
const colors = require('colors')

function getTaxonomies (req, res, next) {
  Taxonomy
    .find({},function(err, tax){
        if(err) res.status(500).send('there was a problem getting taxonomies')
        res.status(200).send(tax).end()
    })
}

function getTaxonomy (req, res, next) {
  Taxonomy.findById(req.params.taxId, function(err, tax) {
    if(err){
      console.log(colors.bgRed(`[ERROR]: ${err}`))
      res.status(500).send('There was a problem finding the taxonomy.')
    }
    res.status(200).send(tax).end()
  })
}

function createTaxonomy(req, res, next){
  if( req.body.name ) {
    const newTax = {
      name: sanitizeHtml(req.body.name),
    }
    Taxonomy.create(newTax, function(err, tax){
      if(err){
        res.status(500).send('Please check your data.')
        console.log(colors.bgRed(`[ERROR]: ${err}`))
      }
      res.status(200).send(tax)
    })
  }
}

function deleteTaxonomy(req, res, next){
  Taxonomy.findByIdAndRemove(req.params.taxId, function(err, tax){ 
    if(err){
      console.log(colors.bgRed(`[ERROR]: ${err}`))
      res.status(500).send('There was a problem deleting the taxonomy')
    }
    res.status(200).send(req.params.id)
    console.log(colors.bgGreen(`[SUCCESS]: ${tax} with the id: ${req.params.id} deleted`))
  })
}

function updateTaxonomy(req, res, next){
    Taxonomy.findById(req.params.taxId, req.body.name, {new: true}, function(err, tax){
      if(err){
        res.status(500).send('There was a problem updating the taxonomy')
      }
      res.status(200).send(tax).end()
    })
}

function createTerm(req, res, next){
  if(req.body.name){
    const newTerm = [{
      _id: new mongoose.Types.ObjectId(),
      name: sanitizeHtml(req.body.name)
    }]
    Taxonomy.findById(req.params.taxId, function(err, tax){
      tax.terms = [...tax.terms, ...newTerm]
      tax.save(function(err){
        if(err) res.status(500).send('There was a problem creating a item menu.')
        res.status(200).send(tax)
      })
    })
  }
}

function deleteTerm(req, res, next){
  if(req.body.termId){
    const termId = sanitizeHtml(req.body.termId)
    Taxonomy.findById(req.params.taxId, function(err, tax){
      const newTerms = tax.terms.filter(term => {
        return term._id != termId
      })
      tax.terms = [...newTerms]
      tax.save(function(err, tax){
        if(err) res.status(500).send('There was a problem deleting an item menu.')
        res.status(200).send(tax)
      })
    }) 
  }
}

function updateTerm(req, res, next){
  if(req.body.termId && req.body.name){
    Taxonomy.findById(req.params.taxId, function(err, tax){
      const newTerms = tax.terms.map(term => {
        if(req.body.termId == term._id){
            term.name = sanitizeHtml(req.body.name)
        }
        return term
      })
      tax.terms = [...newTerms]
      tax.save(function(err, tax){
        if(err) res.status(500).send('There was a problem updating the item menu')
        res.status(200).send(tax)
      })
    })
  }
}

module.exports = {
  getTaxonomies,
  getTaxonomy,
  createTaxonomy,
  deleteTaxonomy,
  updateTaxonomy,
  createTerm,
  deleteTerm,
  updateTerm
}
