const sanitizeHtml = require("sanitize-html")
const bodyParser = require("body-parser")
var Block = require("../../models/Block")
const colors = require('colors')

// get all block elements
function getBlocks(req, res) {
  Block.find({}, function(err, blocks){
    if(err){
      console.log(colors.bgRed(`[ERROR]: ${err}`))
      res.status(500).send('There was a problem getting all blocks')
    }
    res.status(200).send(blocks).end()
  })
}

// get a block element
function getBlock(req, res) {
  Block.findById(req.params.id, function(err, block) {
    if(err){
      console.log(colors.bgRed(`[ERROR]: ${err}`))
      res.status(500).send("there was a problem getting the block")
    }

    res.status(200).send(block).end()
    console.log(colors.bgGreen(`[SUCCESS]: Block ${block.name} retrieved successfuly`))
  })
}

// add a block
function createBlock(req, res){

  if(req.body.name && req.body.description) {
    const NewBlock = {
      name: sanitizeHtml(req.body.name),
      description: sanitizeHtml(req.body.description),
    }

    Block.create(NewBlock, function(err, block){
      if(err) {
        console.log(colors.bgRed(`[ERROR]: ${err}`))
        res.status(500).send('There was a problem creating the block')
      }
      res.status(200).send(block)
      console.log(colors.bgGreen(`[SUCCESS]: block ${block.name} stored in the database`))
    })
  }

}

// delete a block
function deleteBlock(req, res){
  Block.findByIdAndRemove(req.params.id, function(err, block){
    if(err){
      console.log(colors.bgRed(`[ERROR]: ${err}`))
      res.status(500).send('There was a problem deleting the block')
    }
    res.send(req.params).end()
  })
}

function updateBlock(req, res){
  Block.findByIdAndUpdate(req.params.id, req.body, {new: true}, function(err, block){
    if(err){
      console.log(colors.bgRed(`[ERROR]: ${err}`))
      res.status(500).send('There was a problem updating the block')
    }
    res.status(200).send(block)
    console.log(colors.bgGreen(`[SUCCESS]: block ${block.name} updated`))
  })
}

module.exports = {
  getBlocks,
  getBlock,
  deleteBlock,
  createBlock,
  updateBlock
}