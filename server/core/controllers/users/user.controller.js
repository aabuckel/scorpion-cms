const sanitizeHtml = require('sanitize-html')
const argon2 = require('argon2')
const User = require('../../models/User')
const jwt = require('jsonwebtoken')
const key = require('../../config/config')
const { validationResult } = require('express-validator/check')

function getAll(req, res) {
  User.find({}, function(err, users) {
    if (err)
      return res.status(500).send('There was a problem finding the users.')
    res
      .status(200)
      .send(users)
      .end()
  })
}

function getUser(req, res) {
  User.findById(req.params.id, function(err, user) {
    if (err)
      return res.status(500).send('there was a problem retrieving the user')
    res
      .status(200)
      .send(user)
      .end()
  })
}

function addUser(req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.mapped() })
  }
  var newUser = {
    name: sanitizeHtml(req.body.name),
    email: sanitizeHtml(req.body.email),
    password: sanitizeHtml(req.body.password)
  }

  User.create(newUser, function(err, user) {
    if (err) {
      console.log(err)
      return res
        .status(500)
        .send('There was a problem adding an user to the database.')
    }

    var token = jwt.sign({ id: user._id }, key.key, {
      expiresIn: '1hr'
    })
    res.status(200).send({ auth: true, token: token })
  })
}

function deleteUser(req, res) {
  User.findByIdAndRemove(req.params.id, function(err, user) {
    if (err)
      return res.status(500).send('There was a problem deleting the user.')
    res.send(req.params)
  })
}

function updateUser(req, res) {
  User.findByIdAndUpdate(req.params.id, req.body, { new: true }, function(
    err,
    user
  ) {
    if (err)
      return res.status(500).send('There was a problem updating the user.')
    res.status(200).send(user)
  })
}

function authUser(req, res, next) {
  User.findById(req.userId, function(err, user) {
    if (err)
      return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    res.status(200).send(user)
  })
}

function login(req, res) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.mapped() })
  }

  User.findOne({ email: req.body.email }, function(err, user) {
    if (err) {
      return res.status(500).send('Error on the server.')
    }

    if (!user) return res.status(404).send('page not found.')
    var passwordMatch = argon2
      .verify(user.password, req.body.password)
      .then(match => {
        if (!match) {
          return res.status(401).send({ auth: false, token: null })
        } else {
          let payload = { id: user._id, name: user.name }
          let token = jwt.sign(payload, key.key, {
            expiresIn: '1hr'
          })
          res.status(200).send({ auth: true, token: token })
        }
      })
      .catch(err => {
        console.log(err)
      })
  })
}

module.exports = {
  getAll,
  getUser,
  addUser,
  deleteUser,
  updateUser,
  authUser,
  login
}
