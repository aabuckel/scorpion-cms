const sanitizeHtml = require("sanitize-html")
const bodyParser = require("body-parser")
const mongoose = require("mongoose")
const Menu = require('../../models/Menu')


function getMenus(req, res, next){
  Menu.find({}, function(err, menus) {
    if(err) res.status(500).send('There was a problem retrieving all menus.')
    res.status(200).send(menus)    
  })
}

function getMenu(req, res, next){
  Menu.findById(req.params.menuId, function(err, menu){
    if(err) res.status(500).send('There was a problem retrieving the menu.')
    res.status(200).send(menu)  
  })
}

function createMenu(req, res, next){

  if(
    req.body.name &&
    req.body.section &&
    req.body.theme
  ){

    const newMenu = {
      name: sanitizeHtml(req.body.name),
      section: sanitizeHtml(req.body.section),
      theme: sanitizeHtml(req.body.theme)
    }

    Menu.create(newMenu, function(err, menu){
      if(err) res.status(500).send('There was a problem creating the menu.')
      res.status(200).send(menu)
    })

  }
}

function deleteMenu(req, res, next){
  Menu.findByIdAndRemove(req.params.menuId, function(err, menu){
    if(err) res.status(500).send('There was a problem deleting the menu')
    res.status(200).send('Menu deleted')
  })
}

function updateMenu(req, res, next){
  Menu.findByIdAndUpdate(req.params.menuId, req.body, {new: true}, function(err, menu){
    if(err) res.status(500).send('There was a problem updating the menu')
    res.status(200).send(menu)
  })
}

function createItem(req, res, next){
  if(req.body.name && req.body.link){
    const newItem = [{
      _id: new mongoose.Types.ObjectId(),
      name: sanitizeHtml(req.body.name),
      link: sanitizeHtml(req.body.link)
    }]
    Menu.findById(req.params.menuId, function(err, menu){
      menu.items = [...menu.items, ...newItem]
      menu.save(function(err){
        if(err) res.status(500).send('There was a problem creating a item menu.')
        res.status(200).send(menu)
      })
    })
  }
}

function deleteItem(req, res, next){
  if(req.body.itemId){
    const itemId = sanitizeHtml(req.body.itemId)
    Menu.findById(req.params.menuId, function(err, menu){
      const newItems = menu.items.filter(item => {
        return item._id != itemId
      })
      menu.items = [...newItems]
      menu.save(function(err, menu){
        if(err) res.status(500).send('There was a problem deleting an item menu.')
        res.status(200).send(menu)
      })
    }) 
  }
}


function updateItem(req, res, next){
  if(req.body.itemId && req.body.name && req.body.link){
    Menu.findById(req.params.menuId, function(err, menu){
      const newItems = menu.items.map(item => {
        if(req.body.itemId == item._id){
            item.name = sanitizeHtml(req.body.name),
            item.link = sanitizeHtml(req.body.link)
        }
        return item
      })
      menu.items = [...newItems]
      menu.save(function(err, menu){
        if(err) res.status(500).send('There was a problem updating the item menu')
        res.status(200).send(menu)
      })
    })
  }
}


module.exports = {
  getMenus,
  getMenu,
  createMenu,
  deleteMenu,
  updateMenu,
  createItem,
  deleteItem,
  updateItem
}