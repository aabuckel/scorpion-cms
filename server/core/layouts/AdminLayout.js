import React, { Component } from 'react'
import NavigationMenu from '../components/menu/NavigationMenu'
import SidebarMenu from '../components/menu/SidebarMenu'

class AdminLayout extends Component {
  constructor(props) {
    super(props)
    this.state = {
      menuClass: 'closed'
    }
  }

  clickHandler = () => {
    this.setState(function(prevState) {
      return { menuClass: !prevState.menuClass }
    })
  }

  render() {
    return (
      <div className="scp-wrapper">
        <SidebarMenu isOpen={this.state.menuClass} />
        <div>
          <NavigationMenu />
        </div>
        <div className="scp-content">
          <div className="scp-hamburguer-btn" onClick={this.clickHandler}>
            <span></span>
            <span></span>
            <span></span>
          </div>
          <div className="scp-page-content block">{this.props.children}</div>
        </div>
      </div>
    )
  }
}

export default AdminLayout