const express = require('express')

const router = express.Router();
const bodyParser = require('body-parser')
const pageController = require('../../controllers/page/page.controller')

router.use(bodyParser.urlencoded({ extended: true }))
router.use(bodyParser.json())

router.get('/', pageController.getPages)
router.get('/:pageId', pageController.getPage)
router.post('/', pageController.createPage)
router.delete('/:pageId', pageController.deletePage)
router.put('/:pageId', pageController.updatePage)

module.exports = router