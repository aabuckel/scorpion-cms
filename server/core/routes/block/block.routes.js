const express = require('express')

var router = express.Router()
var bodyParser = require('body-parser')
const blockController = require('../../controllers/block/block.controller')

router.use(bodyParser.urlencoded({ extended: true }))
router.use(bodyParser.json())

router.get('/', blockController.getBlocks)
router.get('/:id', blockController.getBlock)

router.post('/', blockController.createBlock)
router.delete('/:id', blockController.deleteBlock)

router.put('/:id', blockController.updateBlock)

module.exports = router