const express = require('express')

const router = express.Router()
const bodyParser = require('body-parser')
const settingsController = require('../../controllers/settings/settings.controller')
const { check } = require('express-validator/check')

router.use(bodyParser.urlencoded({ extended: true }))
router.use(bodyParser.json())

router.get('/', settingsController.getSettings)

router.post(
  '/',
  [
    check('dbName')
      .isLength({ min: 1 })
      .withMessage('must be a name.')
      .trim(),

    check('siteName')
      .isLength({ min: 1 })
      .withMessage('must be a site name.')
      .trim(),

    check('name')
      .isLength({ min: 1 })
      .withMessage('must be a name administrator')
      .trim(),

    check('password')
      .matches(/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/g)
      .withMessage(
        `
        The password field must have:
        Min of 8 characters
        1 letter lowercase at least
        1 letter uppercase at least
        1 character at least
        1 digit at least
      `
      ),

    check('email')
      .isLength({ min: 1 })
      .isEmail()
      .withMessage('must be an email')
      .trim()
      .normalizeEmail()
  ],
  settingsController.createSettings
)

router.put('/:id', settingsController.updateSettings)

module.exports = router
