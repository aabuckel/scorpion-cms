const express = require('express')

const router = express.Router()
const bodyParser = require('body-parser')
const userController = require('../../controllers/users/user.controller')
const { check } = require('express-validator/check')
const VerifyToken = require('../../middlewares/token.middleware')

router.use(bodyParser.urlencoded({ extended: true }))
router.use(bodyParser.json())

// get all users
router.get('/', userController.getAll)
router.get('/me', VerifyToken, userController.authUser)
router.get('/:id', userController.getUser)

router.post(
  '/login',
  [
    check('email')
      .isLength({ min: 1 })
      .isEmail()
      .withMessage('must be an email')
      .trim()
      .normalizeEmail(),

    check('password')
      .matches(/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/g)
      .withMessage(
        `
        The password field must have:
        Min of 8 characters
        1 letter lowercase at least
        1 letter uppercase at least
        1 character at least
        1 digit at least
        `
      )
  ],
  userController.login
)

router.post(
  '/register',
  [
    check('name')
      .isLength({ min: 1 })
      .withMessage('must be a name')
      .trim(),

    check('email')
      .isLength({ min: 1 })
      .isEmail()
      .withMessage('must be an email')
      .trim()
      .normalizeEmail(),

    check('password')
      .matches(/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/g)
      .withMessage(
        `
        The password field must have:
        Min of 8 characters
        1 letter lowercase at least
        1 letter uppercase at least
        1 character at least
        1 digit at least
        `
      )
  ],
  userController.addUser
)

router.delete('/:id', userController.deleteUser)

router.put('/:id', userController.updateUser)

// add the middleware function
router.use(function(user, req, res, next) {
  res.status(200).send(user)
})

module.exports = router
