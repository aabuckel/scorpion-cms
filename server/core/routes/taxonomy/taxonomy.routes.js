const express = require('express')

const router = express.Router()
const bodyParser = require('body-parser')

const TaxonomyController = require('../../controllers/taxonomy/taxonomy.controller')

router.use(bodyParser.urlencoded({ extended: true }))
router.use(bodyParser.json())

router.get('/', TaxonomyController.getTaxonomies)
router.get('/:taxId', TaxonomyController.getTaxonomy)

router.post('/', TaxonomyController.createTaxonomy)
router.put('/:taxId', TaxonomyController.createTerm)

router.delete('/:taxId', TaxonomyController.deleteTaxonomy)
router.put('/:taxId', TaxonomyController.updateTaxonomy)

router.put('/term/:taxId', TaxonomyController.deleteTerm)
router.put('/term/update/:taxId', TaxonomyController.updateTerm)

module.exports = router