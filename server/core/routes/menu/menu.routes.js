const express = require('express')

var router = express.Router()
var bodyParser = require('body-parser')
const menuController = require('../../controllers/menu/menu.controller')

router.use(bodyParser.urlencoded({ extended: true }))
router.use(bodyParser.json())

router.get('/', menuController.getMenus)
router.get('/:menuId', menuController.getMenu)

router.post('/', menuController.createMenu)
router.put('/:menuId',menuController.createItem)

router.delete('/:menuId', menuController.deleteMenu)

router.put('/item/:menuId', menuController.deleteItem)
router.put('/item/update/:menuId', menuController.updateItem)
router.put('/:menuId', menuController.updateMenu)

module.exports = router