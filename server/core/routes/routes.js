const routes = module.exports = require('next-routes')()

const APP_ROUTES = [{
  name: 'index',
  page: 'index',
  pattern: '/'
}, {
  name: 'install',
  page: 'install',
  pattern: '/install'
},
{
  name: 'signUp',
  page: 'signUp',
  pattern: '/signUp'
},
{
  name: 'login',
  page: 'login',
  pattern: '/login'
},
{
  name: 'admin',
  page: 'admin',
  pattern: '/admin'
},{
  name: 'blocks',
  page: 'blocks',
  pattern: '/admin/blocks'
},{
  name: 'pages',
  page: 'pages',
  pattern: '/admin/pages'
},{
  name: 'createPage',
  page: 'createPage',
  pattern: '/admin/pages/create'
}]

APP_ROUTES.map( route => routes.add(route))

