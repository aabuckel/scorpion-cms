import React, { Component } from 'react'
import ReactTable from 'react-table'
import Moment from 'react-moment'

class PagesContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    const { pages } = this.props

    const columns = [
      {
        Header: 'Name',
        accessor: 'name'
      },
      {
        Header: 'Author',
        accessor: 'author'
      },
      {
        Header: 'Date',
        accessor: 'dateCreated',
        Cell: row => {
          return <Moment format="MMM-YYYY-DD HH:mm">{row.value}</Moment>
        }
      },
      {
        Header: 'Url',
        accessor: 'url',
        Cell: row => {
          return <a href="#">{row.value}</a>
        }
      }
    ]

    return (
      <section>
        <h3>Pages Container</h3>
        <ReactTable data={pages} columns={columns} defaultPageSize={10} />
      </section>
    )
  }
}

export default PagesContainer
