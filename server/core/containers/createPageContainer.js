import React, { Component } from 'react'
import { connect } from 'react-redux'
import CKEditor from 'react-ckeditor-component'
import { createPage } from '../store/actions/pages'

class CreatePageContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      content: 'content'
    }
  }

  onSubmit = (e) => {
    e.preventDefault()
    this.props.createPage(this.state)
  }

  onChange = evt => {
    console.log('onChange fired with event info: ', evt)
    var newContent = evt.editor.getData()
    this.setState({
      content: newContent
    })
  }

  onBlur = evt => {
    console.log('onBlur event called with event info: ', evt)
  }

  updateContent = newContent => {
    this.setState({
      content: newContent
    })
  }

  afterPaste = evt => {
    console.log('afterPaste event called with event info: ', evt)
  }

  handleChange = e => {
    let { state } = this
    state[e.target.name] = e.target.value
    this.setState(state)
  }

  render() {
    return (
      <form onSubmit={this.onSubmit}>
        <div className="input-group">
          <label>Name</label>
          <input
            type="text"
            placeholder="name"
            name="name"
            onChange={this.handleChange}
          />
        </div>
        <div className="input-group">
          <label>Description</label>
          <input
            type="text"
            placeholder="description"
            name="description"
            onChange={this.handleChange}
          />
          <small>A simple description which this page is about</small>
        </div>
        <div className="input-group">
          <label>Url</label>
          <input
            type="text"
            placeholder="url"
            name="url"
            onChange={this.handleChange}
          />
          <small>'/about'</small>
        </div>
        <label>Body</label>
        <CKEditor
          activeClass="p10"
          content={this.state.content}
          events={{
            blur: this.onBlur,
            afterPaste: this.afterPaste,
            change: this.onChange
          }}
        />
        <div className="input-group">
          <input className="btn btn-scp-blue" type="submit" name="save" />
        </div>
      </form>
    )
  }
}

function mapStateToProps(state) {
  return {
    pages: state.pagesReducer.pages
  }
}

function mapDispatchToProps(dispatch) {
  return {
    createPage: data => dispatch(createPage(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreatePageContainer)
