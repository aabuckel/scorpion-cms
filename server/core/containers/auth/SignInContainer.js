import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Router, Link } from '../../routes/routes'
import SingInForm from '../../components/auth/SignInForm'
import { login } from '../../store/actions/auth'

class SignInContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  componentWillUpdate(nextProps) {
    if (nextProps.data.token) {
      Router.pushRoute('/admin')
    }
  }

  render() {
    return (
      <div className="form-container">
        <SingInForm onSubmit={this.props.login} onChange={this.onChange} />
        <Link route="/signUp">
          <a>Not a member yet?</a>
        </Link>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    auth: state.loginReducer.auth,
    data: state.loginReducer.data
  }
}

function mapDispatchToProps(dispatch) {
  return {
    login: credentials => dispatch(login(credentials))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignInContainer)
