import React, { Component } from 'react'
import { Router } from '../../routes/routes'

class LogoutContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  onClick = () => {
    document.cookie = 'scp-token' + '=; Max-Age=0'
    document.location.pathname = '/login'
  }

  render() {
    return (
      <button className="btn btn-info" onClick={this.onClick}>
        Logout
      </button>
    )
  }
}

export default LogoutContainer
