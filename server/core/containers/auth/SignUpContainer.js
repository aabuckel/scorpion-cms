import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Router } from '../../routes/routes'
import SingUpForm from '../../components/auth/SignUpForms'
import { register } from '../../store/actions/auth'
import Img from './imgAuth'

class SignUpContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  componentWillUpdate(nextProps) {
    if (nextProps.data.token) {
      Router.pushRoute('/admin')
    }
  }

  render() {
    return (
      <div className="form-container form-auth block">
        <SingUpForm onChange={this.onChange} onSubmit={this.props.register} />
        <div
          className="register-background"
          style={{ backgroundImage: 'url(./static/register.jpg)' }}
        >
          <div>
            <h1>Scorpion CMS</h1>
            <img src="./static/scp-icon-w.png" />
          </div>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    auth: state.registerReducer.auth,
    data: state.registerReducer.data
  }
}

function mapDispatchToProps(dispatch) {
  return {
    register: credentials => dispatch(register(credentials))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUpContainer)
