const DEFAULT_STATE = {
  pages: [],
  isLoading: false,
  err: null
}

const pagesHandler = (state = DEFAULT_STATE, action) => {
  switch(action.type){
    case 'GET_PAGES_REQUEST':
    return {
      ...state,
      isLoading: true
    }
    case 'GET_PAGES_SUCCESS':
      return {
        ...state,
        pages: [...state.pages, ...action.pages],
        isLoading: false
      }
      case 'GET_PAGES_FAILURE':
      return {
        ...state,
        err: action.err,
        isLoading: false
      }
      case 'CREATE_PAGE_SUCCESS':
      return {
        ...state,
        page: [...state.pages, ...action.page]
      }
    default:
      return state  
  }
}

export default pagesHandler