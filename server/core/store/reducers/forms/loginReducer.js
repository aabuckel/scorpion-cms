const DEFAULT_STATE = {
  auth: false,
  data: []
}

const loginHandler = (state = DEFAULT_STATE, action) => {
  switch(action.type) {
    case 'LOGIN_USER_REQUEST':
      return {
        ...state,
        auth: false
      }
    case 'LOGIN_USER_SUCCESS':
      return {
        ...state,
        auth: true,
        data: action.data
      }
    case 'LOGIN_USER_FAILURE':
      return {
        ...state,
        auth: false,
        err: action.err
      }
    default:
      return state
  }
}

export default loginHandler