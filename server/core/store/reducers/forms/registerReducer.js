const DEFAULT_STATE = {
  auth: false,
  data: []
}

const registerHandler = (state = DEFAULT_STATE, action) => {
  switch(action.type) {
    case 'REGISTER_USER_REQUEST':
    return {
      ...state,
      auth: false
    }
    case 'REGISTER_USER_SUCCESS':
      return {
        ...state,
        auth: true,
        data: action.data
      }
    case 'REGISTER_USER_FAILURE':
      return {
        ...state,
        auth: false,
        err: action.err
      }
    case 'LOGIN_USER_REQUEST':
      return {
        ...state,
        auth: false
      }
    case 'LOGIN_USER_SUCCESS':
      return {
        ...state,
        auth: true,
        data: action.data
      }
    case 'LOGIN_USER_FAILURE':
      return {
        ...state,
        auth: false,
        err: action.err
      }
    default:
      return state
  }
}

export default registerHandler