import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import wizardReducer from '../reducers/wizardReducer'
import registerReducer from '../reducers/forms/registerReducer'
import loginReducer from '../reducers/forms/loginReducer'
import pagesReducer from '../reducers/adminPages/pagesReducer'

const rootReducer = combineReducers({
  wizardReducer: wizardReducer,
  form: formReducer,
  registerReducer: registerReducer,
  loginReducer: loginReducer,
  pagesReducer: pagesReducer
})

export default rootReducer