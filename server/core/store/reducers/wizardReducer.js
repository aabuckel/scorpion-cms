const DEFAULT_STATE = {
  counter: 0,
  config: {},
  err: ''
}

const wizardHandler = (state = DEFAULT_STATE, action) => {
  switch (action.type) {
    case 'STEP_INCREMENT':
      return {
        ...state,
        counter: action.counter + 1
      }
    case 'STEP_DECREMENT':
      return {
        ...state,
        counter: action.counter - 1
      }
    case 'SUBMIT_SETTINGS_SUCCESS':
      return {
        ...state,
        config: { ...state.config, ...action.config }
      }
    case 'SUBMIT_SETTINGS_FAILURE':
      return {
        ...state,
        err: action.error
      }
    default:
      return state
  }
}

export default wizardHandler
