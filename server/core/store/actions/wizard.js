const STEP_INCREMENT = 'STEP_INCREMENT'
const STEP_DECREMENT = 'STEP_DECREMENT'

const SUBMIT_SETTINGS_REQUEST = 'SUBMIT_SETTINGS_REQUEST'
const SUBMIT_SETTINGS_SUCCESS = 'SUBMIT_SETTINGS_SUCCESS'
const SUBMIT_SETTINGS_ERRORS = 'SUBMIT_SETTINGS_ERRORS'
const SUBMIT_SETTINGS_FAILURE = 'SUBMIT_SETTINGS_FAILURE'

export function nextStep(counter) {
  return dispatch => {
    dispatch({
      type: STEP_INCREMENT,
      counter: counter
    })
  }
}

export function previousStep(counter) {
  return dispatch => {
    dispatch({
      type: STEP_DECREMENT,
      counter: counter
    })
  }
}

export function setSettings(config) {
  return function(dispatch) {
    dispatch({
      type: SUBMIT_SETTINGS_REQUEST
    })
    fetch(`http://localhost:3000/settings`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(config)
    })
      .then(response => response.json())
      .then(config => {
        document.cookie = `scp-token=${config.token}`
        if (!config.errors) {
          dispatch({
            type: SUBMIT_SETTINGS_SUCCESS,
            config: config
          })
        }
      })
      .catch(error => {
        dispatch({
          type: SUBMIT_SETTINGS_FAILURE,
          error: error
        })
      })
  }
}
