const GET_PAGES_REQUEST = 'GET_PAGES_REQUEST'
const GET_PAGES_SUCCESS = 'GET_PAGES_SUCCESS'
// const GET_PAGES_FAILURE = 'GET_PAGES_FAILURE'

const CREATE_PAGE_REQUEST = 'CREATE_PAGE_REQUEST'
const CREATE_PAGE_SUCCESS = 'CREATE_PAGE_SUCCESS'
const CREATE_PAGE_FAILURE = 'CREATE_PAGE_FAILURE'

export function getPages(pages) {
  return function(dispatch) {
    dispatch({
      type: GET_PAGES_REQUEST
    })
    return {
      type: GET_PAGES_SUCCESS,
      pages: pages
    }
  }
}

export function createPage(data) {
  page
  return function(dispatch) {
    dispatch({
      type: CREATE_PAGE_REQUEST
    })
    fetch('http://localhost:3000/page', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(data)
    })
      .then(response => response.json())
      .then(page => {
        // console.log(page)
        dispatch({
          type: CREATE_PAGE_SUCCESS,
          page: page
        })
      })
      .catch(err => {
        dispatch({
          type: CREATE_PAGE_FAILURE,
          err: err
        })
      })
  }
}
