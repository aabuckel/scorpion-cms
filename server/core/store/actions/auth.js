const REGISTER_USER_REQUEST = 'REGISTER_USER_REQUEST'
const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS'
const REGISTER_USER_FAILURE = 'REGISTER_USER_FAILURE'

const LOGIN_USER_REQUEST = 'LOGIN_USER_REQUEST'
const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS'
const LOGIN_USER_FAILURE = 'LOGIN_USER_FAILURE'

export function register(credentials) {
  return function (dispatch) {
    dispatch({
      type: REGISTER_USER_REQUEST
    })
    fetch(`http://localhost:3000/auth/register`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(credentials),
    })
      .then(response => response.json())
      .then(data => {
        document.cookie = `scp-token=${data.token}`
        dispatch({
          type: REGISTER_USER_SUCCESS,
          data: data
        })
      })
      .catch(error => {
        dispatch({
          type: REGISTER_USER_FAILURE,
          error: error
        })
      })
  }
}

export function login(credentials) {
  return function(dispatch) {
    dispatch({
      type: LOGIN_USER_REQUEST
    });
    fetch("http://localhost:3000/auth/login", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(credentials)
    })
      .then(response => response.json())
      .then(data => {
        document.cookie = `scp-token=${data.token}`
        dispatch({
          type: LOGIN_USER_SUCCESS,
          data: data
        });
      })
      .catch(err => {
        dispatch({
          type: LOGIN_USER_FAILURE,
          err: err
        });
      });
  };
}