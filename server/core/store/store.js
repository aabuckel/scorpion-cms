import thunkMiddleware from "redux-thunk"
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly'
import { createLogger } from "redux-logger"
import { createStore, applyMiddleware} from "redux"
import config from "../config/config"
import rootReducer from '../store/reducers/rootReducer'


function createMiddlewares({ isServer }){

  let middlewares = [thunkMiddleware];
  
  if (config.env === "development" && typeof window !== "undefined") {
    middlewares.push(
      createLogger({
        level: 'info',
        collapsed: true,
        duration: true,
        diff: true
      })
    );
  }
  return middlewares
}

const makeStore = (initialState = {}, context) => {
  let { isServer } = context
  let middlewares = createMiddlewares({ isServer })
  return createStore(
    rootReducer, 
    initialState,
    composeWithDevTools(applyMiddleware(...middlewares))
  )
}

export default makeStore