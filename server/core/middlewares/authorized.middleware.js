const tokenVerifier = require('./token.middleware')

const authorized = async (req, res, next) => {
  try {
    var tetas = await tokenVerifier(req.cookies['scp-token'])
    console.log(tetas)
    return res.redirect('/admin')
  } catch (err) {
    next()
    return
  }
}

module.exports = authorized
