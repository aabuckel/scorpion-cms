var jwt = require('jsonwebtoken')
var key = require('../config/config')

var tokenVerifier = token => {
  return new Promise(resolve => {
    resolve(jwt.verify(token, key.key))
  })
}

module.exports = tokenVerifier
