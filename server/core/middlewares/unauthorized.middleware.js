const tokenVerifier = require('./token.middleware')

const unauthorized = async (req, res, next) => {
  try {
    await tokenVerifier(req.cookies['scp-token'])
    next()
    return
  } catch (err) {
    return res.redirect('/login')
  }
}

module.exports = unauthorized
